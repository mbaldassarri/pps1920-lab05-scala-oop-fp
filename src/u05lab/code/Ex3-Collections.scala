package u05lab.code

import java.util.concurrent.TimeUnit

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer, Map, Set}
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {

  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime() - startTime, TimeUnit.NANOSECONDS)
    if (!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}

object CollectionsTest extends App {

  /* Linear sequences: List, ListBuffer */
  var list: List[Int] = List(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
  println("List size: " + list.size)
  println("List contains element 20: " + list.contains(20))
  println("ListBuffer get element at position 4: " + list(3))
  println("List reverse " + list.reverse)
  println("List add 45 and sort: " + (list :+ 45).sortWith(_ > _))
  // elements cannot be deleted from Immutable List, so a possible solution could be using filter method or drop
  println("List filter: " + list.filter(_ > 30).filter(_ < 80))
  list = list drop 2
  // Deletes the first 2 elements
  println("List Drop: " + list)

  var listBuffer: ListBuffer[Int] = ListBuffer(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
  println(listBuffer)
  println("ListBuffer size: " + listBuffer.size)
  println("ListBuffer get element at position 4: " + listBuffer(3))
  println("ListBuffer update: " + (listBuffer += 45))
  listBuffer(3) = 400
  println("ListBuffer update: " + listBuffer)
  println("ListBuffer delete: " + (listBuffer -= 45))
  listBuffer = listBuffer drop 1
  // Deletes the first element
  println("ListBuffer drop: " + listBuffer)

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  val vector: Vector[Int] = Vector(10, 20, 30, 40, 50, 60)
  println("Vector size: " + vector.size)
  println("Vector get: " + vector(2))
  // replace element at position 2 with 45
  println("Vector update: " + vector.updated(2, 45))
  // elements cannot be deleted from Immutable indexed sequence like Vectors, so a possible solution could be using filter method
  println("Vector delete: " + vector.filter(_ > 30))

  val array: Array[Int] = Array(10, 20, 30) //constant length
  println(array.toList)
  println("Array size: " + array.size)
  println("Array get: " + array(2))
  array(2) = 40
  println("Array update: " + array.toList)

  val arrayBuffer: ArrayBuffer[String] = ArrayBuffer[String]() //fully mutable
  arrayBuffer += ("a", "b", "c")
  println(arrayBuffer)
  println("ArrayBuffer size: " + arrayBuffer.size)
  println("ArrayBuffer get: " + arrayBuffer(2))
  arrayBuffer(2) = "z"
  println("ArrayBuffer update: " + arrayBuffer)
  arrayBuffer += "p"
  println("ArrayBuffer update: " + arrayBuffer)
  println("ArrayBuffer delete: " + (arrayBuffer -= "b"))

  /* Sets */
  var set: Set[Int] = Set(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
  println(set)
  println(set.contains(4)) // true
  println("Set size: " + set.size)
  println("Set read: " + set.find(_ > 3))
  println("Set read: " + set.find(_ == 8))
  println("Set update: " + (set + 20 + 30)) // Cannot update specific element because Set has no indexes. This creates a new Set
  println("Set update: " + (set += 10))
  println("Set remove: " + (set -= 5))

  var immutableSet = scala.collection.immutable.Set(0, 1, 2)
  println(immutableSet.head, immutableSet.size)
  immutableSet += 3
  immutableSet = immutableSet drop 1
  println("Immutable Set: " + immutableSet)

  /* Maps */
  val map: Map[Int, String] = Map(10 -> "hello", 20 -> "world", 30 -> "!!!")  //immutable Map
  println(map(10))
  println(map.size)
  println(map.toVector)
  println("keys: " + map.keys + ", values: " + map.values)
  println(map contains 25)
  val nMap = map + (40->"Hi") // assign the result to a new Map variable
  println(nMap)

  var mutableMap: mutable.Map[Int, String]  = mutable.Map[Int, String](10 -> "hello", 20 -> "world", 30 -> "!!!")
  println(mutableMap.toVector)
  println(mutableMap.get(30)) //returns an Option
  println(mutableMap.size)
  println(mutableMap += (40->"Hi")) // update the current map
  println(mutableMap - 20) // update the current map

  /* Comparison */
  import PerformanceUtils._

  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert(measure("lst last") {
    lst.last
  } > measure("vec last") {
    vec.last
  })
}